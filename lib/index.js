'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _RabbitMQAdapter = require('./RabbitMQAdapter');

var _RabbitMQAdapter2 = _interopRequireDefault(_RabbitMQAdapter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _RabbitMQAdapter2.default;
module.exports = exports['default'];