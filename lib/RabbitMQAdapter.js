'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _amqplib = require('amqplib');

var _amqplib2 = _interopRequireDefault(_amqplib);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Class RabbitMQAdpater
 *
 * Manage Messages Queue Based on RabbitMQ
 */
var RabbitMQAdapter = function () {

  /**
   * Constructor the RabbitMQ Adapter based on given config parameter
   * @param  {Object} config
   * @param  {DatacoreInstance} client DatacoreIntance with Org config and services
   * @return {RabbitMQAdapter}
   */


  /**
   * Default options
   * @type {Object}
   */
  function RabbitMQAdapter(_ref, client) {
    var url = _ref.url,
        options = _ref.options;

    _classCallCheck(this, RabbitMQAdapter);

    this.client = null;
    this.options = {
      noDelay: true
    };
    this.url = null;

    this.client = client;
    this.url = url;
    this.options = _extends({}, this.options, options);
  }

  /**
   * Connect in the RabbitMQ
   * @return {Promise}
   */


  /**
   * RabbitMQ Url Host
   * @type {String}
   */


  /**
   * @type {DatacoreInstance}
   */


  _createClass(RabbitMQAdapter, [{
    key: '_connect',
    value: function _connect() {
      var _this = this;

      if (this.connection) {
        return this.connection;
      }

      this.connection = _amqplib2.default.connect(this.url, this.options);
      var errorCallback = function errorCallback(error) {
        _this.client.verboseLog(error.message);
        _this.connection = null;
      };

      this.connection.then(function (connection) {
        connection.on('close', errorCallback);
        connection.on('error', errorCallback);
      });

      return this.connection;
    }

    /**
     * Resolves to an open Channel. May fail if there are no more channels available
     * @return {Promise}
     */

  }, {
    key: '_createChannel',
    value: function _createChannel() {
      return this._connect().then(function (connection) {
        return connection.createChannel();
      });
    }

    /**
     * Send a single message given as a buffer to the specific exchange named,
     * bypassing routing. The options and return value are exactly
     * the same as for #publish.
     *
     * @param  {String} exchange The Exchange Name
     * @param  {String} routekey The Routekey
     * @param  {Object} message Object with the message
     * @return {Promise}
     */

  }, {
    key: 'emit',
    value: function emit(exchange, arg1, arg2) {
      var routekey = '#';
      var message = null;
      if (arg1 && arg2) {
        routekey = arg1;
        message = arg2;
      } else if (!arg2) {
        message = arg1;
      }

      if (typeof message === 'string') {
        message = {
          message: message
        };
      }

      var newMessage = {
        config: {
          appId: this.client.appId,
          apiKey: this.client.apiKey,
          sessionToken: this.client.sessionToken,
          serverURL: this.client.serverURL
        },
        data: _extends({}, message)
      };
      newMessage = Buffer.from(JSON.stringify(newMessage));

      var exchangeName = this._exchangeName(exchange);

      return this._createChannel().then(function (channel) {
        channel.assertExchange(exchangeName, 'topic', {
          durable: true,
          arguments: {
            'x-message-ttl': 3600000
          }
        });
        channel.publish(exchangeName, routekey, newMessage);
        setTimeout(function () {
          return channel.close();
        }, 1500);

        return true;
      });
    }

    /**
     * Set up a consumer with a callback to be invoked with each message
     * @param  {String} exchange     The Exchange Name
     * @param  {String} routekey     The Routekey
     * @param  {Function} handler Callback for execution
     */

  }, {
    key: 'on',
    value: function on(exchange, arg1, arg2) {
      var _this2 = this;

      var routekey = '#';
      var handler = null;
      if (arg1 && arg2) {
        routekey = arg1;
        handler = arg2;
      } else if (!arg2) {
        handler = arg1;
      }

      var exchangeName = this._exchangeName(exchange);
      var channel = void 0;
      var queue = void 0;

      return this._createChannel().then(function (ch) {
        channel = ch;
        return channel.assertExchange(exchangeName, 'topic', { durable: true });
      }).then(function () {
        return channel.assertQueue('', { autoDelete: true });
      }).then(function (qq) {
        queue = qq.queue;
        return channel.bindQueue(queue, exchangeName, routekey);
      }).then(function () {
        return channel.consume(queue, function (message) {
          try {
            var msg = JSON.parse(message.content.toString());
            handler(null, msg);
          } catch (error) {
            handler(error);
          }
        }, { noAck: true });
      }).then(function () {
        return function () {
          channel.unbindQueue(queue, exchangeName, routekey).then(function () {
            return channel.close();
          }).then(function () {
            return _this2.connection;
          }).then(function (connection) {
            return connection.close();
          });
        };
      }).catch(function (error) {
        return handler(error.message);
      });
    }

    /**
     * Add to exchange name the prefix organization appId
     * @param  {String} exchange The Exchange Name
     * @return {String}
     */

  }, {
    key: '_exchangeName',
    value: function _exchangeName(exchange) {
      var end = '';

      if (process.env.NODE_ENV === 'development') {
        end = '__DEV';
      }

      return this.client.appId + '_' + exchange + end;
    }
  }]);

  return RabbitMQAdapter;
}();

exports.default = RabbitMQAdapter;
module.exports = exports['default'];