import amqp from 'amqplib';

/**
 * Class RabbitMQAdpater
 *
 * Manage Messages Queue Based on RabbitMQ
 */
export default class RabbitMQAdapter {

  /**
   * @type {DatacoreInstance}
   */
  client = null;

  /**
   * Default options
   * @type {Object}
   */
  options = {
    noDelay: true,
  };

  /**
   * RabbitMQ Url Host
   * @type {String}
   */
  url = null;

  /**
   * Constructor the RabbitMQ Adapter based on given config parameter
   * @param  {Object} config
   * @param  {DatacoreInstance} client DatacoreIntance with Org config and services
   * @return {RabbitMQAdapter}
   */
  constructor({ url, options }, client) {
    this.client = client;
    this.url = url;
    this.options = {
      ...this.options,
      ...options,
    };
  }

  /**
   * Connect in the RabbitMQ
   * @return {Promise}
   */
  _connect() {
    if (this.connection) {
      return this.connection;
    }

    this.connection = amqp.connect(this.url, this.options);
    const errorCallback = error => {
      this.client.verboseLog(error.message);
      this.connection = null;
    };

    this.connection.then(connection => {
      connection.on('close', errorCallback);
      connection.on('error', errorCallback);
    });

    return this.connection;
  }

  /**
   * Resolves to an open Channel. May fail if there are no more channels available
   * @return {Promise}
   */
  _createChannel() {
    return this._connect()
      .then(connection => connection.createChannel());
  }

  /**
   * Send a single message given as a buffer to the specific exchange named,
   * bypassing routing. The options and return value are exactly
   * the same as for #publish.
   *
   * @param  {String} exchange The Exchange Name
   * @param  {String} routekey The Routekey
   * @param  {Object} message Object with the message
   * @return {Promise}
   */
  emit(exchange, arg1, arg2) {
    let routekey = '#';
    let message = null;
    if (arg1 && arg2) {
      routekey = arg1;
      message = arg2;
    } else if (!arg2) {
      message = arg1;
    }

    if (typeof message === 'string') {
      message = {
        message,
      };
    }

    let newMessage = {
      config: {
        appId: this.client.appId,
        apiKey: this.client.apiKey,
        sessionToken: this.client.sessionToken,
        serverURL: this.client.serverURL,
      },
      data: {
        ...message,
      },
    };
    newMessage = Buffer.from(JSON.stringify(newMessage));

    const exchangeName = this._exchangeName(exchange);

    return this._createChannel()
      .then(channel => {
        channel.assertExchange(exchangeName, 'topic', {
          durable: true,
          arguments: {
            'x-message-ttl': 3600000,
          },
        });
        channel.publish(exchangeName, routekey, newMessage);
        setTimeout(() => channel.close(), 1500);

        return true;
      });
  }

  /**
   * Set up a consumer with a callback to be invoked with each message
   * @param  {String} exchange     The Exchange Name
   * @param  {String} routekey     The Routekey
   * @param  {Function} handler Callback for execution
   */
  on(exchange, arg1, arg2) {
    let routekey = '#';
    let handler = null;
    if (arg1 && arg2) {
      routekey = arg1;
      handler = arg2;
    } else if (!arg2) {
      handler = arg1;
    }

    const exchangeName = this._exchangeName(exchange);
    let channel;
    let queue;

    return this._createChannel()
      .then(ch => {
        channel = ch;
        return channel.assertExchange(exchangeName, 'topic', { durable: true });
      })
      .then(() => channel.assertQueue('', { autoDelete: true }))
      .then(qq => {
        queue = qq.queue;
        return channel.bindQueue(queue, exchangeName, routekey);
      })
      .then(() => channel.consume(queue, message => {
        try {
          const msg = JSON.parse(message.content.toString());
          handler(null, msg);
        } catch (error) {
          handler(error);
        }
      }, { noAck: true }))
      .then(() => () => {
        channel.unbindQueue(queue, exchangeName, routekey)
          .then(() => channel.close())
          .then(() => this.connection)
          .then(connection => connection.close());
      })
      .catch(error => handler(error.message));
  }

  /**
   * Add to exchange name the prefix organization appId
   * @param  {String} exchange The Exchange Name
   * @return {String}
   */
  _exchangeName(exchange) {
    let end = '';

    if (process.env.NODE_ENV === 'development') {
      end = '__DEV';
    }

    return `${this.client.appId}_${exchange}${end}`;
  }
}
